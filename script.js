import { bankAccount, getLoan } from "./Classes/bank.js";
import { worker, payLoanByPay, transferToBank, goWorking } from "./Classes/work.js";
import { createLaptopList, selectLaptopFeatures, selectedLaptopDetails, buySelectedLaptop } from "./Classes/laptop.js";


/**************************/
//Bank specific
/**************************/
const account = new bankAccount(500,0,false,false);

const lblCash = document.getElementById("lblCash");
const divLoan = document.getElementById("divLoan");
const lblLoan = document.getElementById("lblLoan");
const btnGetLoan = document.getElementById("btnGetLoan");

lblCash.innerText = account.cash;

//GetLoan
const handleGetLoanClick = e =>{
    getLoan(account);
    lblCash.innerText = account.cash;

    divLoan.style.display = "inline";
    lblLoan.innerText = account.loan;
    
}

btnGetLoan.addEventListener("click", handleGetLoanClick);

/**************************/
//Worker specific
/**************************/
const workingMan = new worker(1000);

const lblPay= document.getElementById("lblPay");
const btnLoan = document.getElementById("btnLoan");
const btnWork = document.getElementById("btnWork");

lblPay.innerText = workingMan.pay;

//TransferToBank
const handleTransferClick = e =>{
    transferToBank(account, workingMan);
    lblPay.innerText = workingMan.pay;
    lblCash.innerText = account.cash;
    lblLoan.innerText = account.loan;
}

btnTransfer.addEventListener("click", handleTransferClick);

//PayLoanBack
const handlePaybackClick = e =>{
    payLoanByPay(account, workingMan);

    lblPay.innerText = workingMan.pay;
    lblLoan.innerText = account.loan;
}

btnLoan.addEventListener("click", handlePaybackClick);

//GoWork
const handleWorkClick = e =>{
    goWorking(workingMan);

    lblPay.innerText = workingMan.pay;
}

btnWork.addEventListener("click", handleWorkClick);


/**************************/
//Laptop specific
/**************************/
const laptopList = document.getElementById("laptopList");

//get me the list of all laptops
createLaptopList();

const handleSelectLaptop = e =>{

    
    //fill selection and details panel
    selectLaptopFeatures(laptopList[e.target.selectedIndex].value);
    selectedLaptopDetails(laptopList[e.target.selectedIndex].value);

    //show the button for buying
    btnBuy.style.display="inline";
}

laptopList.addEventListener("change", handleSelectLaptop);

//let me buy one
const handleBuyClick = e =>{

    let selectedLaptopID = document.getElementById("laptopList").value;

    buySelectedLaptop(selectedLaptopID, account);

    lblCash.innerText = account.cash;
    
}

btnBuy.addEventListener("click", handleBuyClick);

