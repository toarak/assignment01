export function  laptop(id, title, description, specs, price, stock, active, image)
    {
        this.id = id;
        this.title = title;
        this.description = description;
        this.specs = specs;
        this.price = price;
        this.stock = stock;
        this.active = active;
        this.image = image;
    }

//Variables Area
let laptopList = []
const laptopListElement = document.getElementById("laptopList");
const featureListElement = document.getElementById("featuresList");
const titelElement = document.getElementById("detailsTitle");
const imageElement = document.getElementById("detailsImage");
const descriptionElement = document.getElementById("detailsDescription");
const priceElement = document.getElementById("detailsPrize");

const apiURL = "https://noroff-komputer-store-api.herokuapp.com"

//Used to make the select List 
export function createLaptopList()
{
    fetch(apiURL +"/computers")
    .then(response => response.json())
    .then(data => laptopList = data)
    .then(laptopList => createLaptopSelector(laptopList))
    .catch(error => console.log('Error: ', error))

    //give me the laptops
    const createLaptopSelector = (laptopList) =>{

        laptopList.forEach(x => addLaptop(x));

    }

    //but only by ID and title
    const addLaptop = (laptop) => {
        const laptopElement = document.createElement("option");
        laptopElement.value = laptop.id;
        laptopElement.appendChild(document.createTextNode(laptop.title));
        laptopListElement.appendChild(laptopElement);
    }
}


//used for the features
export function selectLaptopFeatures(selectedLaptopID)
{
    fetch(apiURL +"/computers")
    .then(response => response.json())
    .then(data => filterById(data, selectedLaptopID))
    .catch(error => console.log('Error: ', error))

    //just give me the one i already selected
    const filterById = (data, selectedLaptopID) => {


        const laptop = data.find(x => x.id == selectedLaptopID);       
        addFeatures(laptop);
    }

    const addFeatures = (laptop) =>{
        
        featureListElement.innerHTML = "";

        const featureList = document.createElement("label");
        featureList.appendChild(document.createTextNode(laptop.specs));
        featureListElement.appendChild(featureList);
    }
}

//used for the details pane
export function selectedLaptopDetails(selectedLaptopID)
{
    fetch(apiURL +"/computers")
    .then(response => response.json())
    .then(data => filterById(data, selectedLaptopID))
    .catch(error => console.log('Error: ', error))

    //just give me the one i already selected
    const filterById = (data, selectedLaptopID) => {

        const laptop = data.find(x => x.id == selectedLaptopID);    
        
        //add infos in details panel
        addImage(laptop);
        addTitle(laptop);
        addDescription(laptop);
        addPrize(laptop);
    }

    //Image
    const addImage = (laptop) =>{

        imageElement.innerHTML = "";

        const image = new Image(100,100);
        image.src = apiURL + "/" + laptop.image;
        imageElement.appendChild(image);
    }

    //Title
    const addTitle = (laptop) =>{

        titelElement.innerHTML = "";

        const title = document.createElement("label");
        title.appendChild(document.createTextNode(laptop.title));
        titelElement.appendChild(title);
    }


    //Description
    const addDescription = (laptop) =>{

        descriptionElement.innerHTML = "";

        const description = document.createElement("label");
        description.appendChild(document.createTextNode(laptop.description));
        descriptionElement.appendChild(description);
    }


    //Prize
    const addPrize = (laptop) =>{

        priceElement.innerHTML = "";

        const prize = document.createElement("label");
        prize.appendChild(document.createTextNode(laptop.price + " kr"));
        priceElement.appendChild(prize);

    }

}

//used for the buying a laptop
export function buySelectedLaptop(selectedLaptopID, bankAccount)
{
    fetch(apiURL +"/computers")
    .then(response => response.json())
    .then(data => buyOne(data, selectedLaptopID, bankAccount))
    .catch(error => console.log('Error: ', error))

    const buyOne = (data, selectedLaptopID, bankAccount) =>{
        
        //just give me the one i already selected
        const laptop = data.find(x => x.id == selectedLaptopID);    
            
        var cash = parseInt(bankAccount.cash);
        var price = parseInt(laptop.price);
        
        //do you have the money?
        if(cash < price){
            window.alert("Insufficient funds");
            return bankAccount;
        }
        //you're good to go
        else{
            bankAccount.cash = cash - price;
            bankAccount.buyed = true;
            window.alert("Congratulation. You are now the owner of this brandnew piece of hightec!");
            return bankAccount;
                        
        }
    }


}

