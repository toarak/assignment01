export function bankAccount(cash, loan, asked, buyed){
    this.cash = cash;
    this.loan = loan;
    this.asked = asked;
    this.buyed = buyed;
}

var cash = 0;
var loan = 0;
var asked = false;
var buyed = false;

//get a loan
export function getLoan(bankAccount){

    let newLoan = 0

    if (!bankAccount.asked){
        let newLoan = parseInt(window.prompt("How many loan do you want?",""));
        
        return checkLoan(bankAccount, newLoan);
    }
    else
    {
        window.alert("Existing loan must be payed back before getting another one.");
        return bankAccount;
    }
    
}

//to check if you can get what you want
function checkLoan(bankAccount, newLoan){

    if (newLoan > parseInt(bankAccount.cash) *2)        
        window.alert("You can't get a loan twice as big as your cash.")

    else{

        bankAccount.cash = parseInt(bankAccount.cash) + newLoan;
        bankAccount.loan = parseInt(bankAccount.loan) + newLoan;
        bankAccount.asked = true;
    }

    return bankAccount;

}

//payback some loan from your account
function payBack(bankAccount){

    let payback = parseInt(window.prompt("How many do you want to pay back?"));

    if (payback > bankAccount.cash){

        window.alert("Insufficient funds.");
        return bankAccount;
    }

    if (bankAccount.loan < payback){

        window.alert("You can't pay more back then you have a loan.");
        return bankAccount;
    }

    bankAccount.loan = parseInt(bankAccount.loan) - payback;
    bankAccount.cash = parseInt(bankAccount.cash) - payback;

    if (bankAccount.loan == 0) 
        bankAccount.asked = false;

    return bankAccount;

}