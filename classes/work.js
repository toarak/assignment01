export function worker(pay){
        this.pay = pay;  
}

var pay = 0;

//pay some loan back with hard earned money
export function payLoanByPay(bankAccount, worker){

    let payback = parseInt(window.prompt("How many do you want to pay back?"));

    //sufficient money?
    if (payback > parseInt(worker.pay)){

        window.alert("Insufficient funds.");
        return bankAccount, worker;
    }

    //too many payback?
    if (parseInt(bankAccount.loan) < payback){

        window.alert("You can't pay more back then you have a loan.");
        return bankAccount, worker;
    }

    bankAccount.loan = parseInt(bankAccount.loan) - payback;
    worker.pay = parseInt(worker.pay) - payback;

    //if no more loan you can get another one
    if (bankAccount.loan == 0) 
        bankAccount.asked = false;

    return bankAccount, worker;

}

//send your money to the bank
export function transferToBank(bankAccount, worker){

    let transfer = parseInt(window.prompt("How many do you want to transfer to your bank account?"));

    //enough money earned?
    if (worker.pay < transfer){
        window.alert("Insufficient funds.");
        return bankAccount, worker;
    }

    worker.pay = parseInt(worker.pay) - transfer;

    //we now you owe one
    if (bankAccount.asked)
    {
        window.alert("10% of this amount will used to pay your outstanding loan.");

        //calculation loan and bank share
        let debts = transfer * 0.1;
        transfer = transfer * 0.9;

        bankAccount.loan = parseInt(bankAccount.loan) - debts;

        //no more or negative loan?
        if (bankAccount.loan <= 0){
            transfer = transfer + (bankAccount.loan * -1);
            bankAccount.loan = 0;
            bankAccount.asked = false;
        }
    }

    bankAccount.cash = parseInt(bankAccount.cash) + transfer;
    return bankAccount, worker;

}

//go peasant, away to work with ya
export function goWorking(worker)
{
    
    worker.pay = parseInt(worker.pay) + 100;
    return worker;
}
